import numpy as np
from scipy import signal
from scipy.signal import savgol_filter as sg




def estimate_noise_per_q(ds, cutoff_rel, pol_order=3, num_intervals=5, lowpass_winsize=11):
    """
    Written by Bianca L. Hansen 2021
    Modified by A. O Dohn 2022.

        Parameters
        ----------
        ds : array
            Difference scattering signal
        cutoff : int
            Value between 0 < cutoff_rel < 1. Cutoff value is scaled with regards to Nyquist sampling fs/2
        pol_order: int, optional
            Order of the filter
        num_intervals: int, optional
            Used to calculate window size. Should be no. of independent points.

        Returns
        -------
        noise_lvl : array (dim of dS)
            An array with the estimated std at each Q-val
        filtered: array (dim of dS)
            the noise part of the signal.
        """
    N_data = len(ds)
    winsize = round(N_data / num_intervals)
    
    sos = signal.butter(pol_order, cutoff_rel, btype='high', analog=False, output='sos', fs=None)
    filtered = signal.sosfilt(sos, ds)
    noise_lvl_tmp = []
    N_steps = len(ds) - winsize
    for i in range(N_steps):
        noise_lvl_tmp.append(np.std(filtered[i:winsize + i], ddof=1))

    std_noiselvl = [noise_lvl_tmp[0]] * (winsize // 2) + noise_lvl_tmp + [noise_lvl_tmp[-1]] * (winsize - winsize // 2)
    noise_lvl = np.array(signal.savgol_filter(std_noiselvl, lowpass_winsize, 1)) # Low pass filtering

    return noise_lvl, filtered





''' Summed Noise Estimation. 
    Here for historical reasons mostly. 

    The new method is much better. 
'''
def estimate_noise(apslist, delay_list, smooth_points=31, qrange=[0, 100]):
    ''' Estimate noise by subtracting Savgol-smoothed curve
        from raw curve. Histogram the difference, fit a gaussian,
        get sigma.

        Parameters:
            apslist (list):
                list of APS objects.
            delay_list (list):
                list of delays to use for noise estimation
                (1 per APS object)
            smooth_points (int, odd):
                number of points to use in the savgol-smooth
    '''
    ads_full = []
    ads_filt = []
    snrs = []
    for a, aps in enumerate(apslist):
        qmask = np.zeros(len(aps.q), bool)
        qmask[(aps.q > qrange[0]) & (aps.q < qrange[1])] = True
        delay = delay_list[a]
        ds_full = np.median(aps.all_ds[delay], axis=0)[qmask]
        ds_filt = sg(ds_full, smooth_points, 3)
        ads_full.append(ds_full)
        ads_filt.append(ds_filt)
        snrs.append(ds_full - ds_filt)

    xmax = max([max(x) for x in snrs])
    xmin = min([min(x) for x in snrs])
    xbins = np.linspace(xmin, xmax, 100)

    fig, ax = plt.subplots(1, 1, figsize=(6, 4));
    for s, snr in enumerate(snrs):
        hist, x = np.histogram(snr, bins=xbins, density=True)
        bin_centres = (x[:-1] + x[1:]) / 2
        p0 = [max(hist), np.mean(snr), np.std(snr)]
        coeff, _ = curve_fit(gauss, bin_centres, hist, p0=p0)
        ax.plot(bin_centres, hist, f'C{s}o', mfc='None', label=apslist[s].tag.replace('_',' '))
        ax.plot(xbins, gauss(xbins, *coeff), f'C{s}-', label=f'STD: {coeff[2]:5.2e}')

    ax.set_ylabel('$\Gamma$(Noise)')
    ax.set_xlabel('Noise lvl')
    ax.set_ylim(bottom=0)
    ax.set_xlim([xmin, xmax])
    ax.legend(loc='best', fontsize=12)
    ax.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
    fig.tight_layout()
    return fig, ax
