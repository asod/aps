import numpy as np
from scipy.signal import savgol_filter as sg
import matplotlib.pyplot as plt
from scipy.optimize import minimize


def standardize_f(x, xmin, xmax):
    return (x - xmin) / (xmax - xmin)

def standardize_b(xnorm, xmin, xmax):
    return xnorm * (xmax - xmin) + xmin


class OptRange:
    def __init__(self, aps, delay, q_range, lo_range, hi_range,
                 sgf=75):
        self.aps = aps
        self.delay = delay
        self.q_range = q_range  # range within to find qmin and qmax
        self.lo_range = lo_range  # range in which signal should be MAXimized
        self.hi_range = hi_range  # range in which signal should be MINimized   
        self.sgf = sgf
        self.iter = 0
    
    def loss(self, x):
        x_actual = standardize_b(x, self.q_range[0], self.q_range[1])
        self.aps.norm_range = [x_actual[0], x_actual[1]]
        self.aps.calc_ds()
        dS = np.median(self.aps.all_ds[self.delay], axis=0)

        lomask = np.zeros(len(self.aps.q), bool)
        himask = np.zeros(len(self.aps.q), bool)

        lomask[(self.aps.q > self.lo_range[0]) & (self.aps.q < self.lo_range[1])] = True 
        himask[(self.aps.q > self.hi_range[0]) & (self.aps.q < self.hi_range[1])] = True

        lo_sum = np.sum(np.abs(sg(dS[lomask], self.sgf, 3)))
        hi_sum = 0 * np.sum(np.abs(sg(dS[himask], self.sgf, 3)))
                     # minimiz.  maximize
        R =  np.sum(1 / lo_sum + hi_sum) / 2
        if self.iter % 10 == 0:
            string = f'Iter: {self.iter:04d}, qmin: {x_actual[0]:5.2f}, ' 
            string += f'qmax: {x_actual[1]:5.2f}, loQ: {1/lo_sum:5.2f}, '
            string += f'hiQ: {hi_sum:5.2f}, Total Residual: {R:g}'
            print(string)
            
        self.iter += 1
        return R

    def fit(self, x0):
        self.first_guess = x0
        x0_std = standardize_f(x0, self.q_range[0], self.q_range[1])
        opt = minimize(self.loss, x0_std, method='Nelder-Mead', 
                       bounds=[(0, 1), (0, 1)],
                       tol=0.0001 * 1e-3) 
        opt.x = standardize_b(opt.x, self.q_range[0], self.q_range[1])
        self.opt = opt
        return opt

    def plot_results(self):
        fig, ax = plt.subplots(1, 1, figsize=(7, 5))
        ax.axhline(0, color='k')
        ax.axvline(self.lo_range[0], color='C0', ls='--')
        ax.axvline(self.lo_range[1], color='C0', ls='--')
        ax.axvline(self.hi_range[0], color='C1', ls='--')
        ax.axvline(self.hi_range[1], color='C1', ls='--')
        self.aps.norm_range = self.first_guess
        self.aps.calc_ds()
        ds = np.median(self.aps.all_ds[self.delay], axis=0)
        ax.plot(self.aps.q, ds, 'C3', lw=3, alpha=0.35)
        ax.plot(self.aps.q, sg(ds, self.sgf, 3), 'C3', lw=2, alpha=1, label='Initial Guess')

        self.aps.norm_range = self.opt.x
        self.aps.calc_ds()
        ds = np.median(self.aps.all_ds[self.delay], axis=0)
        ax.plot(self.aps.q, ds, 'C2', lw=3, alpha=0.35)
        ax.plot(self.aps.q, sg(ds, self.sgf, 3), 'C2', lw=2, alpha=1, 
                label=f'Opt. Range:{self.opt.x[0]:6.4f}-{self.opt.x[1]:6.4f}')
        ax.legend(loc='best', fontsize=12)
        ax.set_xlabel('Q (Å$^{-1}$)')
        ax.set_ylabel('$S(Q)$')
        ax.set_title(self.aps.tag.replace('_', ' '))
        ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        fig.tight_layout()
        ax.set_xlim([self.aps.q.min(), self.aps.q.max()])
        return fig, ax
