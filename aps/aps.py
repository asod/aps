import pyFAI
import fabio
import os, re
import pyFAI.detectors
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
from scipy.io import savemat
from scipy.optimize import curve_fit
from copy import copy
from collections import OrderedDict
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator
import scipy.stats as ss


timescales = {'fs': 1e-15, 'ps':1e-12, 'ns':1e-9, 'us':1e-6, 'ms':1e-3, '':0}

def stack_and_save(list_of_runs):
    ''' Stack list of APS objects, recalculate diff scattering, 
        save to .mat. 

        npys are opened with: 
            aps = np.load(file.npy, allow_pickle=True).item()
        
        NB! APS tags MUST end with '_XXX' where XXX is the run
        number for the renaming to work.. 
    '''
    all_s = np.concatenate([aps.s for aps in list_of_runs])
    all_d = np.concatenate([aps.delays for aps in list_of_runs])
    aps = list_of_runs[0]
    all_ds = aps.calc_ds(all_s, all_d)
    # overwrite first aps object 
    aps.all_ds = all_ds
    aps.delays = all_d
    aps.all_s = all_s
    nums = '-'.join(x for x in [aps.tag.split('_')[-1] for aps in list_of_runs])
    tag = '_'.join(x for x in aps.tag.split('_')[:-1])
    aps.tag = tag + '_'+ nums
    aps.save()


class APS:
    ''' '''
    def __init__(self, tag='pyp2', path="/Users/asod/Dropbox/DTU2/Rejser/2022_APS/test_data",
                 polarization_factor=0.95, re_reduce=False, max_off_dist=10,
                 off_time=-5e-6, n_q_bins=400, mask=None, norm_range=[0.5, 3.4],
                 energy=11.6, center=[1985, 1972], cutoff_factor=20):
        ''' Reduce, filter, and calculate difference-signals from TR-XDS scans 
            at the APS 14ID-B. Might also work elsewhere with a bit of messing around. 
            Reduction by pyFAI.

            Usage: 

            >>> aps = APS()  # init with parameters of choice
            >>> aps.get_delays()  # get delays from log
            >>> aps.set_mask(mask_path)  #  init mask from mask_path
            >>> _ =  aps.reduce(n_imgs='all')  # reduce all images or chose the first N
            >>> # Optional:
            >>> aps.intensity_filter(factor=1.02)  # saves _filtered arrays. See method docu.
            >>> aps.s, aps.delays = aps.s_filtered, aps.delays_filtered
            >>> # calculate difference scattering 
            >>> _ = aps.calc_ds()  # calculate difference signals
            >>> aps.save()  # Saves tag.mat to path/../reduced/

            If you already have reduced some files you can avoid re-reducing them by:
            >>> aps.parse_log()  # to get new images
            >>> aps.get_delays() # get new delays from parsed log
            >>> _ = aps.reduce(n_imgs='all')  # reduce new images
            >>> _ = aps.calc_ds()  # calculate new difference curves
            >>> aps.save() 
            
            Parameters: 
                tag (str):
                    The scan file base-name, e.g. Ru_H2O_1_123 
                    for scan 123 of the Ru_H2O_1 sample
                path (str):
                    Path to the scan files
                off_time (float):
                    The time used for laser-off images
                n_q_bins (int):
                    Number of q bins to use in the azimuthal integration
                polarization factor (float):
                    Polarization factor, from -1 (vertical) to +1 (horizontal), 
                    None for no correction, synchrotrons are around 0.95.
                    See: https://pyfai.readthedocs.io/
                re_reduce (bool):
                    Whether or not to start from first image or only
                    reduce images not already reduced and in object memory
                maxdist (int):
                    Maximum distance to the nearest off before image is 
                    discarded
                norm_range (list):
                    Which Q-range to normalize curves within before 
                    calculating difference signal.
                energy (float):
                    x-ray energy
                center (list):
                    X,Y beamcenter
                cutoff_factor (float):
                    Median filtering cutoff factor

            Where to look first for changing this code to work elsewhere:
                - The hardcoded detector characteristics in init_ai()
                - Other logfile formats, rewrite parse_log() 

        '''
        self.tag = tag
        self.path = path
        self.off_time = off_time  # The delay time we use as off images
        self.n_q_bins = n_q_bins
        self.pf = polarization_factor
        self.re_reduce = re_reduce
        self.maxdist = max_off_dist  # Discard signals with no offs within maxdist 
        self.already_reduced = []
        self.mask = mask
        self.norm_range = norm_range
        self.energy = energy
        self.center = center
        self.cutoff_factor = cutoff_factor
        self.log = None
        self.delays = None
        self.s = None

    def show_corrected_image(self, name):
        ''' Apply Polarization Correction and solid angle 
            correction before showing the image '''
        if self.log == None: 
            _ = self.parse_log()
        img = fabio.open(os.path.join(self.path, name))
        self.init_ai()
        pol = self.ai.polarization(img.data.shape, self.pf)
        sol = self.ai.solidAngleArray(img.shape, True)
        fig, ax = plt.subplots(1, 1, figsize=(10, 6))
        ax.imshow(img.data * pol.data / sol)

    def show_image(self, name):
        ''' Show uncorrected image '''
        img = fabio.open(os.path.join(self.path, name))
        fig, ax = plt.subplots(1, 1, figsize=(10, 6))
        ax.imshow(img.data)
        return fig, ax

    def parse_log(self, log_path=None):
        ''' Parse logfile located at path+tag+.log. 
            Auto-reads and sets detector distance, and
            is used to find list of delays in the scan
        '''

        if log_path == None:
            with open(os.path.join(self.path, self.tag) + '.log', 'r') as f:
                lines = f.readlines()
        else:
            with open(log_path, 'r') as f:
                lines = f.readlines()

        info = {}
        headerct = 0
        for line in lines:
            if 'detector distance' in line:
                print(line)
                self.det_dist = float(line.split()[3]) * 1e-3
            if line[0] == '#':
                headerct += 1
            else:
                continue
        log = {x:[] for x in lines[headerct - 1 ].split('\t')}
        for l, line in enumerate(lines[headerct:]):
            data = line.split('\t')
            for k, key in enumerate(log.keys()):
                log[key].append(data[k])
        self.log = log
        return log

    def get_delays(self):
        ''' Gets delays as float from log entry '''
        if self.log == None: 
            _ = self.parse_log()
        delays = [re.split('(\d+)', x) for x in self.log['delay']]
        self.delays = [float(''.join(delay[:-1])) * timescales[delay[-1]] for delay in delays]
        # Avoid annoying .9999999999999, round to nearest fs
        self.delays = np.array([np.round(x, 15) for x in self.delays])

    def set_mask(self, path):
        ''' Add the mask to the APS() object'''
        self.mask = fabio.open(path)

    def init_ai(self):
        ''' Initialize the pyFAI AzimuthalIntegrator object. 
            Pixel size is currently hardcoded in here... 
        '''
        detector = pyFAI.detectors.RayonixMx340hs()  # APS bioCARS detector
        if self.mask != None:
            detector.guess_binning(self.mask.data)

        ai = AzimuthalIntegrator(dist=self.det_dist, 
                detector=detector, wavelength=12.35 / self.energy * 1e-10)

        ai.poni1 = self.center[0] * 89e-6
        ai.poni2 = self.center[1] * 89e-6
        self.ai = ai

    def reduce(self, n_imgs='all'):  # WIP: only pick a range or a special delay..
        ''' Reduce 2D images to 1D absolute scattering curves (applying mask).
            returns q vector, s, and sigmas, where
                s: (N, q) array, N = number of curves, q = length of q vector
                sigmas: same shape array of variances'''
        self.init_ai()

        if self.log == None: 
            _ = self.parse_log()

        if self.re_reduce:
            self.already_reduced  = []

        if n_imgs == 'all': 
            n_imgs = len(self.delays)
        else:
            self.delays = self.delays[:n_imgs]

        s = np.zeros((n_imgs, self.n_q_bins))
        sigmas = np.zeros((n_imgs, self.n_q_bins)) 
        print('Reducing')
        if len(self.already_reduced) == 0:
            # get a 2theta as well
            file = self.log['file'][0]
            img = fabio.open(os.path.join(self.path, file))
            theta, _, _ = self.ai.integrate1d(np.asarray(img.data, dtype=float) - 9.75, 
                        self.n_q_bins, unit="2th_deg", error_model="poisson",
                        mask=self.mask.data, polarization_factor=self.pf, correctSolidAngle=True)
            self.theta = theta            
        for f, file in tqdm(enumerate(self.log['file']), total=n_imgs):
            if f >= n_imgs:
                break
            if file in self.already_reduced:
                s[f, :] = self.s[f, :]
                sigmas[f, :] = self.sigmas[f, :]        
                q = self.q
                continue
            img = fabio.open(os.path.join(self.path, file))
            q, I, sigma = self.ai.integrate1d(np.asarray(img.data, dtype=float) - 9.75, self.n_q_bins, 
                                         unit="q_A^-1", error_model="poisson",
                                         mask=self.mask.data, 
                                         polarization_factor=self.pf, correctSolidAngle=True)
            s[f, :] = I
            sigmas[f, :] = sigma        
            self.already_reduced.append(file)

        self.s = s
        self.sigmas = sigmas 
        self.q = q
        return q, s, sigmas

    def calc_ds(self, s=None, delays=None, nearest_off_neighbours=2):
        ''' Calculate difference scattering curves. 
            Returns dict of (N, q) arrays, where N
            is the number of diff curves for that delay time.
            dict keys are the unique delays in the scan
        '''

        nob = nearest_off_neighbours

        if (self.s is None) and (s is None):
            _, __, ___ = self.reduce()
        if s is None:
            s = self.s
            delays = self.delays

        maxdist = self.maxdist
        scale_mask = np.zeros(len(self.q), bool)
        scale_mask[(self.q > self.norm_range[0]) & (self.q < self.norm_range[1])] = True
        offs = np.isclose(delays, self.off_time, atol=1e-15)  # mask
        off_idxs = np.where(offs)[0]
        unique_delays = np.unique(delays) 
        all_ds = {this_time:None for this_time in unique_delays}
        for this_time in unique_delays:
            on_idxs = np.where(delays == this_time)[0]
            ds = np.zeros((len(on_idxs), len(self.q)))
            discard_mask = np.zeros(len(on_idxs), bool)
            for o, on_idx in enumerate(on_idxs):
                c2 = abs(on_idx - off_idxs).argsort()[:nob]  # find closest n
                offs = s[off_idxs[c2], :]

                # Discard this diff if offs are more than maxdist away:
                imgdists = on_idx - off_idxs[c2]
                discard = np.abs(imgdists) > maxdist 
                if discard.all():
                    discard_mask[o] = True
                    continue

                norm_offs = offs / np.median(offs[:, scale_mask], axis=1)[:, None]  
                if nob > 1:
                    weighed = np.abs(imgdists) / np.abs(imgdists).sum()
                    norm_off = (weighed[:, None] * norm_offs).sum(0)  # weigh with distance from on
                else:
                    norm_off = norm_offs.sum(0)  
                norm_on = s[on_idx, :] / np.median(s[on_idx, scale_mask])
                diff = norm_on - norm_off
                ds[o, :] = diff
                self.offs = offs
            all_ds[this_time] = ds[~discard_mask] 
            if discard_mask.any():
                num_disc = sum(discard_mask)
                tot_crvs = len(discard_mask)
                print(f'{this_time:10.2e}: Discarded {num_disc:3d} curves out of {tot_crvs:3d} total')
        self.all_ds = all_ds
        return(all_ds)

    def median(self):   
        for delay, ds in self.all_ds.items():
            rms_list = np.sum((ds - np.median(ds, axis=0))**2, axis=1)
            mask = rms_list < min(rms_list) * self.cutoff_factor
            self.all_ds[delay] = self.all_ds[delay][mask]
            print(f'Delay: {delay:10.2e}s, Filtering out {sum(~mask):d} curves')

    def intensity_filter(self, factor = 1.02, stepsize=3, plot=True):
            ''' Rejects images with summed intensity higher
                than the two neighbouring images. Recalc difference signals after
                application (or apply before calculating diffs)
            '''
            summed_list = np.sum(self.s, 1)
            mask = np.zeros(len(summed_list), bool)
            for i in range(len(summed_list)):
                if i  < stepsize:
                    vicinity = np.asarray([*summed_list[i + 1:i + 1 + stepsize * 2]])
                elif i > len(summed_list) - stepsize:
                    vicinity = np.asarray([*summed_list[i - stepsize * 2:i]])
                else:
                    vicinity = np.asarray([*summed_list[i - 3:i],
                                           *summed_list[i + 1:i + i + stepsize]])
                    
                int_ratio = abs(summed_list[i] / np.median(vicinity))
                relative = np.max([int_ratio, 1 / int_ratio]) > factor 
    
                mask[i] = relative                
                
            print(f'FILTERED OUT {sum(mask)} curves (red circles show removed imgs)')
            if plot:
                xaxis = np.arange(len(summed_list))
                fig, axes = plt.subplots(1, 2, figsize=(8, 5))
                ax = axes[0]
                ax.plot(xaxis, summed_list, 'k.')
                ax.plot(xaxis[mask], summed_list[mask], 'ro', mfc='None')
                ax.set_xlabel('Img#')
                ax.set_ylabel('Itot')
                ax = axes[1]
                for i in range(self.s.shape[0]):
                    ax.plot(self.q, self.s[i, :], f'C{int(mask[i])}', alpha=0.05)
                for ax in axes:
                    ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
                ax.plot([], [], 'C0', label='Kept')
                ax.plot([], [], 'C1', label='Discarded')
                ax.legend(loc='best')
                ax.set_xlabel('Q (Å$^{-1}$)')
                ax.set_ylabel('S(Q)')
                ax.yaxis.tick_right()
                ax.yaxis.set_label_position("right")
                fig.tight_layout()
                tit = self.tag.replace('-', '')
                fig.suptitle(f'{tit}')
    
            self.s_filtered = self.s[~mask]
            self.delays_filtered = self.delays[~mask]
            self.sigmas_filtered = self.sigmas[~mask]
            self.already_reduced_filtered = [ar for ar, m in zip(self.already_reduced, mask) if ~m]
            
            return summed_list


    def save(self, outtag=''):
        data = {'q': self.q,
                'theta':self.theta,
                'AllTTDelay': np.vstack([np.median(val, axis=0) for key, val in self.all_ds.items()]),
                'all_ds': {str(key).replace('-', 'm'):val for key, val in self.all_ds.items()},
                'delay_list': self.delays,
                'delays':[key for key in self.all_ds.keys()],
                'all_s': self.s,
                'norm_range':self.norm_range}
        os.makedirs(os.path.join(self.path, '..', 'reduced'), exist_ok=True)
        savemat(os.path.join(self.path, '..', 'reduced', self.tag) + outtag + '.mat', data)

        # copy self and remove heavy and unpickable things from it 
        out_self = copy(self)
        out_self.mask = []
        out_self.ai = []
        np.save(os.path.join(self.path, '..', 'reduced', self.tag) + outtag + '.npy', out_self)


    def count_imgs(self, show=True):
        uni_d, counts =  np.unique(self.delays, return_counts=True)
        if show:
            for d, c in zip(uni_d, counts):
                print(f'Delay: {d:9.2e} #imgs: {c:d}')
        return uni_d, counts



def gauss(x, *p):
    A, mu, sigma = p
    return A * np.exp(-(x - mu)**2 / (2. * sigma**2))

