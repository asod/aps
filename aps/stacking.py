import glob

import numpy as np

from collections import OrderedDict


def stack(path, delays, scans, 
          norm_range=[0.7, 3.4], 
          nearest_off_neighbours=2):
    ''' Stack diff scattering from list of APS objects.

        Output: 
           APS object with stacked diff scattering, 
           absolute scattering and delays
    '''

    apslist = []
    for scan in scans:  
        scanname = glob.glob(path+f'*_{scan:03d}.npy')[0]
        apslist.append(np.load(scanname, allow_pickle=True).item())
    
    all_ds_scans = [] 
    for aps in apslist:
        aps.norm_range = norm_range
        all_ds_scans.append(aps.calc_ds(nearest_off_neighbours=nearest_off_neighbours))

    all_ds_stacked = {} 
    for delay in delays:
        all_ds_stacked[delay] = np.concatenate([all_ds[delay] 
                                                for all_ds in all_ds_scans])

    all_s = np.concatenate([aps.s for aps in apslist])
    all_delays = np.concatenate([aps.delays for aps in apslist])

    for d, delay in enumerate(delays):
        if d ==0:
           rej_delays = all_delays == delay
        else:
           rej_delays += all_delays == delay

    # take the first aps object, overwrite all_ds
    aps = apslist[0]
    aps.all_ds = all_ds_stacked
    aps.s = all_s[rej_delays]
    aps.delays = all_delays[rej_delays]

    return aps

