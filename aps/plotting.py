import numpy as np
from scipy.optimize import curve_fit
from collections import OrderedDict
import matplotlib.pyplot as plt 
from scipy.signal import savgol_filter as sg
import scipy.stats as ss

strtimes = {'-06':'us', '-07':'00 ns', '-08':'0 ns', 
            '-09':' ns', '-10':'00 ps', '-11':'0 ps', '-12':' ps'}

def make_colors(c, colmap='viridis'):
    cmap = plt.get_cmap(colmap)
    colors = [cmap(1. * i / c) for i in range(c)]
    return colors


def plot_img_chunks(aps, delay, bin_imgs=50, 
                    sgf=11, xlim=[0, 5], ylim=None, figsize=(7,4),
                    legend_entries=4, return_bins=False):

    ds = aps.all_ds[delay]
    images = len(ds)
    
    bin_edges = np.arange(0, images, bin_imgs)
    
    fig, ax = plt.subplots(1, 1, figsize=(7, 5));
    #ax.set_title(aps.tag.replace('-', '')+ ' %s' % delay)
    
    col = make_colors(len(bin_edges))

    binned_signals = []
    
    for i, start in enumerate(bin_edges):
        if len(ds) > start + bin_imgs:
            end = start + bin_imgs
        else:
             end = len(ds)   
        medi = np.median(ds[start:end], axis=0) 
        binned_signals.append(medi)
        ax.plot(aps.q, medi, alpha=0.35, lw=3, color=col[i])
        ax.plot(aps.q, sg(medi, sgf, 3), color=col[i], label='%d-%d' %(start, end))
    
        ax.legend(loc='best')
        ax.set_xlabel('Q (Å$^{-1}$)')
        ax.set_ylabel('$\Delta S(Q)$')

    h, l, = ax.get_legend_handles_labels()
    chunk = int(np.ceil(len(h) / legend_entries)) 
    ax.legend(h[::chunk], l[::chunk], loc='best')

    fig.tight_layout()
    if return_bins:
        return fig, ax, binned_signals
    else:
        return fig, ax


def plot_curves(aps, delays=None, sgf=31, xlim=[0, 5], ylim=None,
                figsize=(8, 7)):
    ''' Plot dS curves on top of each other '''
    fig, ax = plt.subplots(1, 1, figsize=figsize);
    col = make_colors(len(aps.all_ds))
    od = OrderedDict(sorted(aps.all_ds.items()))
    if delays == None:
        delays = [key for key in od.keys()]

    for d, (delay, ds) in enumerate(od.items()):
        if delay not in delays:
            continue
        medi = np.median(ds, axis=0) #*aps.q
        time = str(delay).split('e')
        ax.plot(aps.q, medi, '-', alpha=0.35, lw=3, 
                color=col[d])
        ax.plot(aps.q, sg(medi, sgf, 3), lw=2, color=col[d],
                label=f'{time[0]}{strtimes[time[1]]}')
    ax.set_xlabel('Q (Å$^{-1}$)')
    ax.set_ylabel('$\Delta$S(Q)')
    ax.set_title(aps.tag.replace('-', '') + f' norm: {aps.norm_range[0]}-{aps.norm_range[1]}', fontsize=12)
    ax.set_xlim(xlim)
    if ylim is not None:
        ax.set_ylim(ylim)
    ax.legend(loc='upper right', ncol=2, fontsize=12)
    fig.tight_layout()
    return fig, ax


def plot_wfall(aps, offset=0.0005, sgf=31, xlim=[0, 5], ylim=None, 
              figsize=(7, 15), delays=None):
    ''' Waterfall-style plot of dS-curves shifted vertically with 
        `offset` from each other.  '''

       
    od = OrderedDict(sorted(aps.all_ds.items()))
    col = make_colors(len(aps.all_ds))
    if delays == None:
        delays = [key for key in od.keys()]
    fig, ax = plt.subplots(1, 1, figsize=figsize);
    for d, (delay, ds) in enumerate(od.items()):
        if delay not in delays:
            continue
        medi = np.median(ds, axis=0)
        ax.plot(aps.q, medi + d * offset, '-', alpha=0.35, lw=3,
                 color=col[d], label=f'{delay*1e12:3.0f} ps, {ds.shape[0]:4d} imgs')
        ax.plot(aps.q, sg(medi, sgf, 3) + d * offset, lw=2, color=col[d])
        time = str(delay).split('e')
        ax.text(xlim[1] +.02, d * offset,
                f'{time[0]}{strtimes[time[1]]}\n{ds.shape[0]:3d} imgs',
                va='center', fontsize=10)
        ax.axhline(d * offset, color='black')
    ax.set_xlabel('Q (Å$^{-1}$)')
    ax.set_ylabel('$\Delta$S(Q)')
    ax.set_title(aps.tag.replace('-', ''))
    ax.set_xlim(xlim)
    if ylim is not None:
        ax.set_ylim(ylim)
    fig.tight_layout()
    return fig, ax


def plot_2d(aps, smooth=5, contrast=25, q_limits=None):
    ''' 2D plot of difference-scattering, X: Time, Y: Q.

        Parameters:
            aps (APS object):
                The run object you want to plot
            smooth (int):
                The number of points to use in sgolay filtering along Q.
                5 is basically no filtering (using 3rd order poly.)
            constrast (int):
                Color bar scale.
            q_limits (list):
                [qmin, qmax] to plot
    
    '''
    od = OrderedDict(sorted(aps.all_ds.items()))
    times = np.array([key for key in od.keys()])
    fig = plt.figure(figsize=(7, 5))
    ax = plt.gca()
    ts, qs = np.meshgrid(times, aps.q)
    diffs = np.concatenate([sg(np.median(value, axis=0), smooth, 3)
                                for value in od.values()]).reshape(len(times), len(aps.q))
    #diffs = np.concatenate([np.median(value, axis=0)
    #                            for value in od.values()]).reshape(len(times), len(aps.q))
    plt.pcolormesh(ts, qs, diffs.T, shading='auto')

    timeticks = [str(time).split('e')[0] + strtimes[str(time).split('e')[1]] for time in times]
    ax.set_xticks(times)
    ax.set_xticklabels(timeticks, rotation=45, ha='right')

    ax.set_xlim(min([t for t in times if t != aps.off_time]), 
                max([t for t in times if t != aps.off_time]))
    cbar = plt.colorbar()
    limit_m = 100 / contrast
    qq = list(range(len(aps.q)))
    mad = ss.median_abs_deviation
    clims = [np.median(diffs[:, qq]) - limit_m * mad(diffs[:, qq], axis=None), 
             np.median(diffs[:, qq]) + limit_m * mad(diffs[:, qq], axis=None)]
    plt.clim(clims)
    if q_limits is None:
        q_limits = [min(aps.q), max(aps.q)]
    ax.set_ylim(q_limits)
    cbar.formatter.set_powerlimits((0, 0))
    cbar.set_label('$\Delta S(Q)$')
    ax.set_title(aps.tag.replace('_', ' '))
    ax.set_ylabel('Q (Å$^{-1}$)')
    ax.set_xlabel('Time')
    fig.tight_layout()
    return fig, ax


def plot_motor(apslist, mopo, delay, qrange=[0.5, 3], sgf=31,
               do_fit=False, xlabel='LaserX'):
    ''' Optimize some motor position at a fixed delay, over
        scans in apslist. Get summed abs intensity within qrange
        for each 'scan' at a certain motor position.

        Parameters:
            apslist (list):
                list of APS objects.
            mopo (list):
                list of motor positions
            delay (float):
                which delay to use
            qrange (list)
                which q interval to use for summing
            sgf (int):
                how many points to apply Savgol over before
                taking abs and sum - to avoid noise artificially
                increasing perceived signal strength.
           do_fit (bool):
                For power titration. Will fit a line through the
                points to help visualize if we are within the
                linear regime of laser excitation or not.
           xlabel (str):
                Name of motor you want on the plot
    '''
    sgnl = np.zeros(len(apslist))
    qmin = qrange[0]
    qmax = qrange[1]
    fig, ax = plt.subplots(1, 1, figsize=(7, 5));
    for r, aps in enumerate(apslist):
        all_ds = aps.calc_ds()
        scale_mask = np.zeros(len(aps.q), bool)
        scale_mask[(aps.q > qmin) & (aps.q < qmax)] = True
        ds = all_ds[delay]
        medi = np.median(ds, axis=0)
        # Summed absolute signal strength
        sgnl[r] = np.sum(np.abs(sg(medi[scale_mask], sgf, 3)))
        print(f'{aps.tag}, images: {all_ds[delay].shape[0]:3d}, Signal Strength: {sgnl[r]}')
        
        lab = aps.tag.split('_')[-1] + f':, {mopo[r]}'

        ax.plot(aps.q, medi, f'C{r}', alpha=0.35, lw=3)
        ax.plot(aps.q, sg(medi, sgf, 3), f'C{r}', label=lab)

    ax.axvline(qmin, color='k')
    ax.axvline(qmax, color='k')
    ax.legend(loc='best')
    ax.set_xlabel('Q (Å$^{-1}$)')
    ax.set_ylabel('$\Delta S(Q)$')
    fig.tight_layout()


    sort = np.argsort(mopo)
    fig, ax = plt.subplots(1, 1, figsize=(7, 5))
    ax.plot(mopo[sort], sgnl[sort], 'kx')
    if do_fit:
        a0 = (sgnl[-1] - sgnl[0]) / (mopo[-1] - mopo[0])
        f = lambda x, *p: p[0] * x
        fit = curve_fit(f, mopo, sgnl, [a0])

        x = np.linspace(mopo.min(),
                        mopo.max(), 100)
        ax.plot(x, f(x, fit[0]), 'k--')

    ax.set_xlabel(xlabel)
    ax.set_ylabel(f'Absolute Summed Int in {qmin} < Q < {qmax}')
    ax.set_xticks(mopo)
    fig.tight_layout()
    return fig, ax
