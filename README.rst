APS Tools
=========

Reduce, filter, and calculate difference-signals from TR-XDS scans
at the APS 14ID-B. Might also work elsewhere with a bit of messing around.
Reduction by pyFAI.


Repo organization:
------------------
The `main` branch contains the newest version of reduction and online analysis
tools, but *nothing* else. Each beamtime has it's own branch. At the end of 
each beamtime:  

- Merge the changes and additions to the core tools into main,   

- Clean out the online work notebooks and other mess  

- Add usage examples of new tools to `APS2022.ipynb`  


Previous beamtimes and their branches:
--------------------------------------
- `levi2022`: GUP-75534 (HTI Beamtime, Sep2022)
- `MeBPY_GUP72792`: GUP-72792 (FeBPY/RuBPY Beamtime, Apr2022)


Installation:
-------------
You basically just need `aps/` somewhere in your $PYTHONPATH.  
Dependencies (all installable via `pip`):

- `pyFAI <https://pypi.org/project/pyFAI/>`_   
- `fabio <https://pypi.org/project/fabio/>`_    
- `tqdm <https://pypi.org/project/tqdm/>`_     
- matplotlib  


Usage:
------
>>> from aps.aps import APS
>>> aps = APS()  # init with parameters of choice  
>>> aps.get_delays()  # get delays from log  
>>> aps.set_mask(mask_path)  #  init mask from mask_path  
>>> _ =  aps.reduce(n_imgs='all')  # reduce all images or chose the first N  
>>> # Optional:  
>>> aps.intensity_filter(factor=1.02)  # saves _filtered arrays. See method docu.  
>>> aps.s, aps.delays = aps.s_filtered, aps.delays_filtered  
>>> # calculate difference scattering  
>>> _ = aps.calc_ds()  # calculate difference signals  
>>> aps.save()  # Saves tag.mat to path/../reduced/  
>>> 
>>> If you already have reduced some files you can avoid re-reducing them by:  
>>> aps.parse_log()  # to get new images  
>>> aps.get_delays() # get new delays from parsed log  
>>> _ = aps.reduce(n_imgs='all')  # reduce new images  
>>> _ = aps.calc_ds()  # calculate new difference curves  
>>> aps.save()  


For simple usage examples, look in `APS2022.ipynb`. If you're feeling 
adventurous, go through the online analysis notebooks hidden in the 
beamtime branches. 


Making Masks:
-------------
https://pyfai.readthedocs.io/en/master/man/pyFAI-drawmask.html
